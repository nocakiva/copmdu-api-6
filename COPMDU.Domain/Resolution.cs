﻿using Dapper.Contrib.Extensions;

namespace COPMDU.Domain
{
    public class Resolution
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int AtlasId { get; set; }
        public string AtlasDescription { get; set; }
        public bool Active { get; set; }
    }
}
