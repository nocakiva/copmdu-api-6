﻿using COPMDU.Infrastructure.ClassAttribute;
using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Domain.Notification
{
    public class Cell
    {
        [PageResultProperty(@"&pCe_Codigo=(.+?)&pTipoRel=", ValueType = PageResultPropertyAttribute.ConversionType.String)]
        public string Name { get; set; }

        [PageResultProperty(@"<td>[\w\W]+?</td>\n<td align=""center"">\n<small>\s+?(\d+?)</small>", ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
        public int Ie1Count { get; set; }

        [PageResultProperty(@"<td>[\w\W]+?</td>\n<td align=""center"">[\w\W]+?</td>\n<td align=""center"">\n<small>\s+?(\d+?)</small>", ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
        public int Ie2Count { get; set; }

        [PageResultProperty(@"<td>[\w\W]+?</td>\n<td align=""center"">[\w\W]+?</td>\n<td align=""center"">[\w\W]+?</td>\n<td align=""center"">\n<small>\s+?(\d+?)</small>", ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
        public int Ie3Count { get; set; }

        [PageResultProperty(@"<td>[\w\W]+?</td>\n<td align=""center"">[\w\W]+?</td>\n<td align=""center"">[\w\W]+?</td>\n<td align=""center"">[\w\W]+?</td>\n<td align=""center"">\n<small>\s+?(\d+?)</small>", ValueType = PageResultPropertyAttribute.ConversionType.Integer)]
        public int Ie4Count { get; set; }
    }
}
